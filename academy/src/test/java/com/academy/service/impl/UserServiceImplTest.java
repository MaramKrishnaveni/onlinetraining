package com.academy.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.academy.dto.EnrollmentResponseDto;
import com.academy.entity.Enrollment;
import com.academy.entity.User;
import com.academy.exception.UserIdNotFoundException;
import com.academy.repository.EnrollmentRepository;
import com.academy.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class UserServiceImplTest {
	@Mock
	UserRepository userRepository;
	@Mock
	EnrollmentRepository enrollmentRepository;
	@InjectMocks
	UserServiceImpl userServiceImpl;
	private final Integer userId = 1;
	Enrollment enrollment;
	List<Enrollment> listOfEnrollment;
	User user;

	@BeforeAll
	public void setup() {
		enrollment = new Enrollment();
		listOfEnrollment = new ArrayList<>();
		enrollment.setCourseId(1);
		enrollment.setEnrollmentId(1);
		enrollment.setStatus("scheduled");
		enrollment.setUserId(1);
		listOfEnrollment.add(enrollment);
		user = new User();
		user.setUserId(1);

	}

	@Test
	void showAllEnrollmentsOfUserTest() throws UserIdNotFoundException {

		Mockito.when(userRepository.findByUserId(userId)).thenReturn(Optional.of(user));
		Mockito.when(enrollmentRepository.findByUserId(userId)).thenReturn(Optional.of(listOfEnrollment));
		
		Optional<List<EnrollmentResponseDto>> result = userServiceImpl.showAllEnrollment(userId);
		assertNotNull(result);
	}
}
