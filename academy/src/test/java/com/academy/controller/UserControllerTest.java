package com.academy.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.academy.dto.EnrollmentResponseDto;
import com.academy.exception.UserIdNotFoundException;
import com.academy.service.UserService;

@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class UserControllerTest {
	@Mock
	UserService userService;
	@InjectMocks
	UserController userController;
	private final Integer userId = 1;
	EnrollmentResponseDto enrollmentResponseDto;
	List<EnrollmentResponseDto> listOfenrollmentResponseDto;

	@BeforeAll
	public void setup() {

		enrollmentResponseDto = new EnrollmentResponseDto();
		listOfenrollmentResponseDto = new ArrayList<>();
		enrollmentResponseDto.setCourseId(1);
		enrollmentResponseDto.setEnrollmentId(1);
		enrollmentResponseDto.setStatus("scheduled");
		enrollmentResponseDto.setUserId(1);
		listOfenrollmentResponseDto.add(enrollmentResponseDto);

	}

	@Test
	void getAllEnrollmentsOfUserTest() throws UserIdNotFoundException {

		Mockito.when(userService.showAllEnrollment(userId)).thenReturn(Optional.of(listOfenrollmentResponseDto));

		 ResponseEntity<Optional<List<EnrollmentResponseDto>>> result = userController.viewAllEnrollments(userId);
		assertNotNull(result);
	
	}
}
