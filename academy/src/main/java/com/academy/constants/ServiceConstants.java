package com.academy.constants;

public class ServiceConstants {

	private ServiceConstants() {
		throw new IllegalStateException("Constants class");
	}
	
	public static final String APPROVED = "APPROVED";
	public static final String INPROGRESS = "INPROGRESS";
	public static final String REJECTED = "REJECTED";
	public static final String REFER_BACK = "REFER BACK";
	public static final String SUCCESSFUL_STATUS_UPDATE = "STS-200";
	public static final String NO_RECORDS_FOUND = "No Records Found.";
	
	public static final String USERID_NOT_FOUND = "userId is not found";
}
