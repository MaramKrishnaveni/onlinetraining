package com.academy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.academy.entity.Enrollment;

public interface ScheduleRepository extends JpaRepository<Enrollment, Integer> {

}
