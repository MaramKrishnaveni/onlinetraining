package com.academy.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.academy.entity.Enrollment;
import com.academy.entity.ScheduleCalender;
import com.academy.entity.User;

@Repository
public interface EnrollmentRepository extends JpaRepository<Enrollment, Integer>{

	Optional<List<Enrollment>> findByUserId(Integer userId);

	}
