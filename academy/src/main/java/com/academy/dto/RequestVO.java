package com.academy.dto;

import com.sun.istack.NotNull;

public class RequestVO {
	
	@NotNull
	private int adminId;
	@NotNull
	private String connectionStatus;
	@NotNull
	private int requestId;
	/**
	 * @return the adminId
	 */
	public int getAdminId() {
		return adminId;
	}
	/**
	 * @param adminId the adminId to set
	 */
	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}
	/**
	 * @return the connectionStatus
	 */
	public String getConnectionStatus() {
		return connectionStatus;
	}
	/**
	 * @param connectionStatus the connectionStatus to set
	 */
	public void setConnectionStatus(String connectionStatus) {
		this.connectionStatus = connectionStatus;
	}
	/**
	 * @return the requestId
	 */
	public int getRequestId() {
		return requestId;
	}
	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	
	
	@Override
	public String toString() {
		return "ProvideConRequestVO [connectionStatus=" + connectionStatus+ "]";
	}
}
