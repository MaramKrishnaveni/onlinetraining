package com.academy.exception;

import javax.security.auth.login.AccountNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.academy.dto.ErrorResponseDto;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = UserIdNotFoundException.class)
	public ResponseEntity<ErrorResponseDto> handleUserIdNotFoundException(
			UserIdNotFoundException userIdNotFoundException) {
		ErrorResponseDto errorResponseDto = new ErrorResponseDto();
		errorResponseDto.setStatusCode("error-601");
		errorResponseDto.setStatusMessage(userIdNotFoundException.getMessage());
		return new ResponseEntity<>(errorResponseDto, HttpStatus.BAD_REQUEST);
	}

	
	
	
}
