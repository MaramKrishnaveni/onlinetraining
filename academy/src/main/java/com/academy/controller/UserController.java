package com.academy.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.academy.dto.EnrollmentResponseDto;
import com.academy.exception.UserIdNotFoundException;
import com.academy.service.UserService;

/**
 * This controller is handling the requests and responses for user operations.
 * 
 * @author SQUAD 3
 * @since 2020/11/28
 */
@RestController
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	UserService userService;

	/**
	 * Method to call service method for requesting to view enrollment history
	 * 
	 * @param userId
	 * @return Optional<EnrollmentResponseDto> which consist of connection
	 *         userId,enrollmentId,status courseId
	 * @exception UserIdNotFound
	 * 
	 */

	@GetMapping("/users/{userId}/enrollments")
	public ResponseEntity<Optional<List<EnrollmentResponseDto>>> viewAllEnrollments(
			@PathVariable @Valid @NotNull(message = "userId should not be null") @Min(value = 1, message = "Please provide the valid userId") Integer userId)
			throws UserIdNotFoundException {

		return new ResponseEntity<>(userService.showAllEnrollment(userId), HttpStatus.OK);

	}

}
