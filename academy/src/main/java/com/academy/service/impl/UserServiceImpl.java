package com.academy.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.academy.constants.ServiceConstants;
import com.academy.dto.EnrollmentResponseDto;
import com.academy.entity.Enrollment;
import com.academy.entity.User;
import com.academy.exception.UserIdNotFoundException;
import com.academy.repository.EnrollmentRepository;
import com.academy.repository.UserRepository;
import com.academy.service.UserService;

/**
 * The serviceImpl method.
 * 
 * @author SQUAD 3
 * @since 2020/11/28
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	EnrollmentRepository enrollmentRepository;

	/**
	 * Method to call service method for requesting to view enrollment history
	 * 
	 * @param userId
	 * @return Optional<EnrollmentResponseDto> which consist of connection
	 *         userId,enrollmentId,status courseId
	 * @exception UserIdNotFound
	 * 
	 */
	@Override
	public Optional<List<EnrollmentResponseDto>> showAllEnrollment(Integer userId) throws UserIdNotFoundException {

		Optional<User> userDetails = userRepository.findByUserId(userId);
		if (!userDetails.isPresent()) {
			throw new UserIdNotFoundException(ServiceConstants.USERID_NOT_FOUND);
		}
		Optional<List<Enrollment>> result = enrollmentRepository.findByUserId(userDetails.get().getUserId());

		List<EnrollmentResponseDto> responseDto = new ArrayList<>();
		result.get().stream().forEach(listOfEnrollment -> {
			EnrollmentResponseDto dto = new EnrollmentResponseDto();
			dto.setCourseId(listOfEnrollment.getCourseId());
			dto.setEnrollmentId(listOfEnrollment.getEnrollmentId());
			dto.setStatus(listOfEnrollment.getStatus());
			dto.setUserId(listOfEnrollment.getUserId());
			responseDto.add(dto);

		});

		return Optional.of(responseDto);

	}

}
