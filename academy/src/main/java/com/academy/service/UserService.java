package com.academy.service;

import java.util.List;
import java.util.Optional;

import com.academy.dto.EnrollmentResponseDto;
import com.academy.exception.UserIdNotFoundException;
/**
 * This service  method.
 * 
 * @author SQUAD 3
 * @since 2020/11/28
 */
public interface UserService {

	/**
	 * Method to call service method for requesting to view enrollment history
	 * 
	 * @param userId
	 * @return Optional<EnrollmentResponseDto> which consist of connection
	 *         userId,enrollmentId,status courseId
	 * @exception UserIdNotFound
	 * 
	 */
	public Optional<List<EnrollmentResponseDto>> showAllEnrollment(Integer userId) throws UserIdNotFoundException;
}
